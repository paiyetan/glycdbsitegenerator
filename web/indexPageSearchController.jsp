<%-- 
    Document   : indexPageSearchController
    Created on : Oct 10, 2014, 11:24:57 AM
    Author     : paiyeta1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% 
    String searchType;
    String searchItem;
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search controller</title>
    </head>
    <body>
    <%
        searchType = request.getParameter("searchType");
        searchItem = request.getParameter("searchItem");
        if(searchType.equalsIgnoreCase("Peptide")){
            response.sendRedirect("peptideResponse.jsp?searchItem="+searchItem);
        }else if(searchType.equalsIgnoreCase("Protein")){
            response.sendRedirect("proteinResponse.jsp?searchItem="+searchItem);
        }
    %>       
    </body>
</html>
