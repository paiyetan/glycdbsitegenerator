/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author paiyeta1
 */
public class IDPeptidesTableBean {
    private boolean allTableRowsLoaded;
    private int currentTableCapacity;

    public IDPeptidesTableBean() {
        this.allTableRowsLoaded = false;
        this.currentTableCapacity = 1000;
    }
    
    public boolean isAllTableRowsLoaded() {
        return allTableRowsLoaded;
    }

    public void setAllTableRowsLoaded(boolean allTableRowsLoaded) {
        this.allTableRowsLoaded = allTableRowsLoaded;
    }

    public int getCurrentTableCapacity() {
        return currentTableCapacity;
    }

    public void setCurrentTableCapacity(int currentTableCapacity) {
        this.currentTableCapacity = currentTableCapacity;
    }
    
    
}
