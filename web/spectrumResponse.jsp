<%-- 
    Document   : spectrumResponse
    Created on : Oct 8, 2014, 12:40:41 PM
    Author     : paiyeta1
--%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Identified spectrum</title>
        <link rel="stylesheet" href="bin/css/resultstyle.css"> 
        <link REL="stylesheet" TYPE="text/css" HREF="bin/css/lorikeet.css">
        <script>
            var newwindow;
            function openNewWindow(url)
            {
                newwindow=window.open(url,'name', 'height=550,width=850,top=100,left=100,resizable=yes,scrollbars=yes,toolbar=yes,status=yes');
                if (window.focus) {newwindow.focus()}
                //newwindow.resizeTo(screen.availWidth,screen.availHeight);
            }           
        </script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="../js/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.min.js"></script>
        <script type="text/javascript" src="bin/js/jquery.flot.js"></script>
        <script type="text/javascript" src="bin/js/jquery.flot.selection.js"></script>

        <script type="text/javascript" src="bin/js/specview.js"></script>
        <script type="text/javascript" src="bin/js/peptide.js"></script>
        <script type="text/javascript" src="bin/js/aminoacid.js"></script>
        <script type="text/javascript" src="bin/js/ion.js"></script> 
        <%!
            String setPeaksString(String mzArrString, String mzIntensityArrString){
                String peaks;
                String[] mzArr = mzArrString.split(",");
                String[] intArr = mzIntensityArrString.split(",");
                /*
                peaks = "[ ";
                for(int i = 0; i < mzArr.length; i++){
                    String keyValuePair = "[" + mzArr[i] + "," + intArr[i] + "]";
                    //keyValuePairList.add("[" + mzArr[i] + "," + intArr[i] + "]");
                    peaks = peaks + keyValuePair;
                    if(i < (mzArr.length-1)){
                        peaks = peaks + "," ;
                    }
                    if(i == (mzArr.length-1)){
                        peaks = peaks + " ]" ;
                    }
                }
                */
                peaks = "";
                for(int i = 0; i < mzArr.length; i++){
                    String keyValuePair = mzArr[i] + " " + intArr[i];
                    //keyValuePairList.add("[" + mzArr[i] + "," + intArr[i] + "]");
                    peaks = peaks + keyValuePair;
                    if(i < (mzArr.length-1)){
                        //peaks = peaks + "\n" ;
                        peaks = peaks + "_" ;
                    }
                    //if(i == (mzArr.length-1)){
                    //    peaks = peaks + " ]" ;
                    //}
                }
                System.out.println(peaks);
                return peaks;
            }
        
        %>
    </head>
    <script>
        function getPeaks(peaksStr){
            // -----------------------------------------------------------
            // Get the peaks:
            // -----------------------------------------------------------
            var peaks  = [];
            //var peaksStr = $("#peaks").val();
            //var peaksArr = peaksStr.split("\n");
            var peaksArr = peaksStr.split("_");
            for(var i = 0; i < peaksArr.length;	 i += 1) {
                var val = $.trim(peaksArr[i]);
                if(val.length == 0) // skip over empty lines
                        continue;
                var peakVals = val.split(/\s+/);
                if(peakVals.length != 2) {
                        alert("Invalid peak data: "+peakVals);
                        return false;
                }
                peaks.push([parseFloat(peakVals[0]), parseFloat(peakVals[1])]);
            }
            return peaks;
        }
    </script>
    <body>
        <div id="result" class="spectrum">
            <%
              String spectrumId = request.getParameter("spectrumId");
              String mappedGlycosite = request.getParameter("mappedGlycosite");
              String matchedPeptide = request.getParameter("matchedPeptide");//load database...
              //make queries on database and place result in Java Objects...
              // SpectrumID
              // precursorIonCharge
              // precursorIonMz
              // precursorIonRT
              // precursorIonIntensity
              
              //access db for glycosite (peptide) associated information...
              Connection connection; 
              Statement statement;
              ResultSet resultSet; 
              String query;
              
              int precursorIonCharge = 0;
              double precursorIonMz = 0;
              double precursorIonRT = 0;
              double precursorIonIntensity = 0;
              String mzArrString = "";
              String mzIntensityArrString = "";
              
              String peaksString = "";
              String fileName = spectrumId.split("\\.")[0];
              String scanNumber = spectrumId.split("\\.")[1];
              
              
              //try{
                  Class.forName("org.sqlite.JDBC");
                  connection = DriverManager.getConnection("jdbc:sqlite:" + 
                          "C:/Users/paiyeta1/Projects/cptacassociated/analysis/glyco/glycotriplicatesorbitr-2014.03.04/text/xGlycscan.tables/dbase/glycoSQLiteDB.db");
                  statement = connection.createStatement();  

                  // retrieve particular spectrum information(s)...
                  query = "SELECT * FROM spectra_info WHERE _spectrumId='" + 
                            spectrumId + "';"; 
                  resultSet = statement.executeQuery(query); 
                  while(resultSet.next()){
                      precursorIonCharge = resultSet.getInt("_precursor_ion_charge");
                      precursorIonMz = resultSet.getDouble("_precursor_ion_mz");
                      precursorIonRT = resultSet.getDouble("_precursor_ion_rt");
                      precursorIonIntensity = resultSet.getDouble("_precursor_ion_intensity");
                      mzArrString = resultSet.getString("_mz_array");
                      mzIntensityArrString = resultSet.getString("_intensity_array");
                      peaksString = setPeaksString(mzArrString, mzIntensityArrString);
                  } 
                  resultSet.close();
                  statement.close();
                  connection.close(); 
                  
                  if(mappedGlycosite==null || matchedPeptide==null){
                      //retrieve mapped glycosite from database table 
                      // glycosite_spectrum_map[ _id, _glycosite, _spectrumId ]
                      Class.forName("org.sqlite.JDBC");
                      connection = DriverManager.getConnection("jdbc:sqlite:" + 
                              "C:/Users/paiyeta1/Projects/cptacassociated/analysis/glyco/glycotriplicatesorbitr-2014.03.04/text/xGlycscan.tables/dbase/glycoSQLiteDB.db");
                      statement = connection.createStatement();  

                      // retrieve particular spectrum information(s)...
                      query = "SELECT * FROM glycosite_spectrum_map WHERE _spectrumId='" + 
                                spectrumId + "';"; 
                      resultSet = statement.executeQuery(query); 
                      while(resultSet.next()){
                          mappedGlycosite = resultSet.getString("_glycosite");                        
                      } 
                      resultSet.close();
                      statement.close();
                      connection.close(); 
                  }
            %>           
            <h3>Spectrum ID: <%= spectrumId %></h3>
            <strong>Mapped glycosite (peptide):</strong><%= mappedGlycosite %><br>
            <strong>Precursor ion <i>RT</i>:</strong><%= precursorIonRT %><br>
            <strong>Precursor ion <i>[absolute] intensity</i>:</strong><%= precursorIonIntensity %><br>             
        </div>
        <div id="fig">
            <hr>
            <!-- <strong>Spectrum</strong> -->
            <!-- PLACE HOLDER DIV FOR THE SPECTRUM -->
            <div id="lorikeet"></div>           
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                    var sequence = '<%= mappedGlycosite %>';
                    var scanNumber = <%= scanNumber %>;
                    var charge = <%= precursorIonCharge %>;
                    var precursorMz = <%= precursorIonMz %>;
                    var fileName = '<%= fileName %>';
                    var varMods = [];//
                    // modification index = 14; modification mass = 16.0; modified residue = 'M'
                    //varMods[0] = {index: 14, modMass: 16.0, aminoAcid: 'M'};
                    // mass to be added to the N-terminus
                    var ntermMod = 144.102063; //defaults to iTRAQ4plex modification
                    // peaks in the scan: [m/z, intensity] pairs.
                    //var peaks = '<%= peaksString %>';
                    var peaks = getPeaks('<%= peaksString %>');
                    /* render the spectrum with the given options */
                    $("#lorikeet").specview({sequence: sequence, 
                                             scanNum: scanNumber,
                                             charge: charge,
                                             precursorMz: precursorMz,
                                             fileName: fileName,
                                             //staticMods: staticMods, 
                                             variableMods: varMods, 
                                             ntermMod: ntermMod, 
                                             //ctermMod: ctermMod,
                                             peaks: peaks//,
                                             //width: 600,
                                             //height: 450         
                                             });	
            });
                
        </script>    
    </body>
</html>
