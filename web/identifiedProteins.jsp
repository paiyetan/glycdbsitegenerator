<%-- 
    Document   : identifiedProteins
    Created on : Oct 8, 2014, 2:57:25 PM
    Author     : paiyeta1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import="org.sqlite.*" %>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedList"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Mass Spectrometry Identified Glycosites/Peptides Database [Identified Proteins]</title>
        <meta name="description" content="A mass spectrometry based glycopeptide database...">
        <!--<link rel="stylesheet" href="main.css"> -->
        <link rel="stylesheet" href="bin/css/style.css"> 
        
        <%!
            //load database
            //export table
            //retrive elements
            //access db for glycosite (peptide) associated information...
            Connection connection = null; 
            Statement statement = null;
            ResultSet resultSet = null; 
            String query = null;
          
          
          void loadTable(javax.servlet.jsp.JspWriter out)//, beans.IDPeptidesTableBean idPeptideTable) 
                  throws java.io.IOException, ClassNotFoundException, SQLException {
              //int tableCapacity = idPeptideTable.getCurrentTableCapacity();
              out.println("<table>");
			out.println("<tr class=\"title\">");
                            out.println("<td colspan=\"3\" align=\"center\">");
                                out.println("<strong>Identified Proteins</strong>");
                            out.println("</td>");
			out.println("</tr>");
                        out.println("<tr class=\"header\">");
                            out.println("<td >Index</td>");
                            out.println("<td><strong>Protein Accession</strong></td>");
                            out.println("<td><strong>Full Name</strong></td>");
                        out.println("</tr>");
  
                       Class.forName("org.sqlite.JDBC");
                    connection = DriverManager.getConnection("jdbc:sqlite:" + 
                            "C:/Users/paiyeta1/Projects/cptacassociated/analysis/glyco/glycotriplicatesorbitr-2014.03.04/text/xGlycscan.tables/dbase/glycoSQLiteDB.db");
                    statement = connection.createStatement();  
                    query = "SELECT * FROM unique_proteins"; 
                    resultSet = statement.executeQuery(query); 
                    //int tableRow = 0;
                    while(resultSet.next()){
                        //tableRow++;
                        int index = resultSet.getInt("_id");
                        String protAccn = resultSet.getString("_protein_accession");
                        String protFullName = resultSet.getString("_protein_fullname");
                        //if(index <= tableCapacity){
                            out.println("<tr>");
				out.println("<td align=\"right\">");
					out.println(index);
				out.println("</td>");
				out.println("<td align=\"left\">");
					out.print("<a href=\"javascript:openNewWindow('proteinResponse.jsp?searchItem=" + protAccn + "');\">"); //out.print(queryString); out.println("'");                       
                                            out.println(protAccn);
                                        out.println("</a>");
				out.println("</td>");
				out.println("<td align=\"left\">");
                                //String queryString = "javascript:openNewWindow('peptideResponse.jsp?searchItem=" + cseq + "')"; 
                                out.println(protFullName); 
                                        out.println("</td>");
                            out.println("</tr>");  
                        //}
                    }
                    //if(resultSet.isAfterLast()){
                    //    idPeptideTable.setAllTableRowsLoaded(true);
                    //}else
                    out.println("</table>"); 
                    resultSet.close();
                    statement.close();
                    connection.close();                           
          }        
        %> 
        <script>
            var newwindow;
            function openNewWindow(url)
            {
                newwindow=window.open(url,'name','height=550,width=850,top=100,left=100,resizable=yes,scrollbars=yes,toolbar=yes,status=yes');
                if (window.focus) {newwindow.focus()}
                //newwindow.resizeTo(screen.availWidth,screen.availHeight);
            }           
        </script>       
    </head>
    <body style="align-items: center">
        <div id="page_bg"> <%-- style="width: 90, align: center"> --%>
            <div id="top_compartment">
                <div id="banner">
                    <a href=""><img src="images/OvGlycDBHeader.jpg" align="center"/></a>
                </div>
                <div id="nav">
                    <p>
                        <h3> <a href="index.jsp">Home</a> | <a href="identifiedProteins.jsp">Proteins</a> | <a href="identifiedPeptides.jsp">Peptides</a> | <a href="identifiedSpectra.jsp">Spectra</a> | <a href="downloads.jsp">Downloads</a></h3>
                        <hr>
                    </p>
                </div>                
            </div>
            <div id="middle_compartment">
                <div id="middle_compartment_left-column">
                </div>
                <div id="middle_compartment_center-column">
                    <%
                       loadTable(out);                   
                    %>                           
                </div>
                <div id="middle_compartment_right-column">
                </div>
            </div>
            <%--
            <div id="footer_compartment">
                <div id="copyright">
                    <p>&copy; Copyright 2014</p>
                    <p id="date"></p>
                </div>
                <div id="contact">
                    <hr>
                    Department of Pathology | Johns Hopkins University | Baltimore | MD 21212
                </div>         
            </div>
           --%>
        </div>
        <script>
            document.getElementById("date").innerHTML = Date();
        </script>
    </body>
</html>
