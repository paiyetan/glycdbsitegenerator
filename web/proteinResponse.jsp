<%-- 
    Document   : proteinResponse
    Created on : Oct 8, 2014, 12:40:23 PM
    Author     : paiyeta1
--%>

<%@page import="java.util.Set"%>
<%@page import="others.GlycProteinMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Identified Protein</title>
        <link rel="stylesheet" href="bin/css/resultstyle.css">
        <link rel="stylesheet" type="text/css" href="bin/css/pviz-core.css">
        <script src="bin/js/pviz-bundle.min.js"></script>
        <script src="bin/js/sms_common.js"></script>
        <script>
            var newwindow;
            function openNewWindow(url)
            {
                newwindow=window.open(url,'name','height=550,width=850,top=100,left=100,resizable=yes,scrollbars=yes,toolbar=yes,status=yes');
                if (window.focus) {newwindow.focus()}
                //newwindow.resizeTo(screen.availWidth,screen.availHeight);
            }           
        </script>
     </head>
    <body>
        <div id="result">
            <%
              String proteinAccn = request.getParameter("searchItem");
              //load database...
              //make queries on database and place result in Java Objects...
              // proteinId
              //   ..protein Fullname
              //   ..mapped glycosites...
              
              //   ..protein sequence (with identified glycosite locations highlighted)
              LinkedList<String> mappedGlycosites = new LinkedList<String>();
              HashMap<String,GlycProteinMap> glycToGlycProteinMap =
                      new HashMap<String,GlycProteinMap>();
              //LinkedList<String> mappedSpectraIds = new LinkedList<String>();
              
              //access db for glycosite (peptide) associated information...
              Connection connection; 
              Statement statement;
              ResultSet resultSet; 
              String query;
              String proteinFullName = "";
              String proteinSequence = "";
              
              //try{
                  Class.forName("org.sqlite.JDBC");
                  connection = DriverManager.getConnection("jdbc:sqlite:" + 
                          "C:/Users/paiyeta1/Projects/cptacassociated/analysis/glyco/glycotriplicatesorbitr-2014.03.04/text/xGlycscan.tables/dbase/glycoSQLiteDB.db");
                  statement = connection.createStatement();  
                  // retrieve protein fullname...
                  query = "SELECT * FROM unique_proteins WHERE _protein_accession='" + 
                            proteinAccn + "';"; 
                  resultSet = statement.executeQuery(query);                  
                  while(resultSet.next()){
                      proteinFullName = resultSet.getString("_protein_fullname");               
                  }             
                  // retrieve mapped glycosites...
                  query = "SELECT * FROM glycosite_protein_map WHERE _protein_accession='" + 
                            proteinAccn + "';";
                  resultSet = statement.executeQuery(query); 
                  while(resultSet.next()){
                      int id = resultSet.getInt("_id");
                      String mappedGlycosite = resultSet.getString("_glycosite_sequence");
                      String location = resultSet.getString("_location");
                      mappedGlycosites.add(mappedGlycosite);
                      glycToGlycProteinMap.put(mappedGlycosite, 
                                                new GlycProteinMap(id, mappedGlycosite, proteinAccn,
                                                                    location));
                  }
                  // retrieve protein sequence...
                  query = "SELECT _sequence FROM protein_sequence_map WHERE _accession='" + 
                            proteinAccn + "';";
                  resultSet = statement.executeQuery(query); 
                  proteinSequence = resultSet.getString("_sequence");

                  resultSet.close();
                  statement.close();
                  connection.close();
              //} catch (Exception ex){
              //    ex.printStackTrace();
              //}
            %>
        <table>
            <tr class="header">
                <td colspan="2"><h3>Identified protein accession: <%= proteinAccn %></h3></td></tr>
            <tr valign="top">
                <td><strong>Full name:</strong></td>
                <td><%= proteinFullName %></td>
            </tr>
            <tr valign="top">
                <td><strong>Mapped peptide(s)/glycosite(s):</strong></td>
                <td>
                    <% for(String mappedGlycosite: mappedGlycosites){ 
                        String queryString = "javascript:openNewWindow('peptideResponse.jsp?searchItem=" + mappedGlycosite + "');";
                    %>    
                        <a href=<%= queryString %> ><%= mappedGlycosite %></a><br>
                    <% } %>
                </td>
            </tr>
            
        </table>
        </div>
        <div id=fig">
            <hr>
            <strong>Protein sequence</strong>
            <div id="browser">               
                <%--
                https://github.com/Genentech/pviz
                http://www.ncbi.nlm.nih.gov/pubmed/25147360              
                --%>
            </div>
            <script>
                var pviz = this.pviz;
                var seqEntry = new pviz.SeqEntry({sequence : '<%= proteinSequence %>'});


                new pviz.SeqEntryAnnotInteractiveView({
                  model : seqEntry,
                  el : '#browser'
                  }).render();
                <%-- 
                    <% GlycProteinMap gpm = glycToGlycProteinMap.get(mappedGlycosites.getFirst()); %>  
                --%>
                seqEntry.addFeatures([
                    <%--
                // {category : 'foo', type : 'bar', start : 5, end : 12, text : 'hello'},
                // {category : 'glycosite', type : 'bar', start : '<%= gpm.getStart() %>', end : '<%= gpm.getEnd() %>', text : '<%= mappedGlycosites.getFirst() %>'}
                     --%>
                <% for(String mappedGlycosite: mappedGlycosites){ 
                     GlycProteinMap gpm = glycToGlycProteinMap.get(mappedGlycosite); %>
                     {category : 'glycosites', type : 'bar', start : '<%= gpm.getStart() - 1 %>', end : '<%= gpm.getEnd() - 1 %>', text : '<%= mappedGlycosite %>'},               
                <% } %>                
                ]);
                                     
            </script>
            <div id="sequence">
                <hr>
                <%
                 char[] seqArr = proteinSequence.toCharArray();
                 int blocksize = 10;
                 int lineSize = 70;
                 int index = 0;
                 while(index < seqArr.length){
                     if((index % lineSize)==0){ // && index > 0){
                         if(index!=0)
                            out.print(seqArr[index] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + index + "<br>");// + "\n");
                         // out.println();
                     }else if((index % blocksize)==0){
                         out.print(seqArr[index] + "&nbsp;&nbsp;&nbsp;&nbsp;");
                     }else{
                         out.print(seqArr[index]);
                     }
                     index++;
                 }
                %>              
            </div>
            
        </div>        
    </body>
</html>
