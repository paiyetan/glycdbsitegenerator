<%-- 
    Document   : index
    Created on : Oct 8, 2014, 11:40:35 AM
    Author     : paiyeta1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Mass Spectrometry Identified Glycosites/Peptides Database</title>
        <meta name="description" content="A mass spectrometry based glycopeptide database...">
        <!--<link rel="stylesheet" href="main.css"> -->
        <link rel="stylesheet" href="bin/css/style.css">  
        <script>
            var newwindow;
            function openNewWindow(url)
            {
                newwindow=window.open(url,'name','height=550,width=850,top=100,left=100,resizable=yes,scrollbars=yes,toolbar=yes,status=yes');
                if (window.focus) {newwindow.focus()}
                //newwindow.resizeTo(screen.availWidth,screen.availHeight);
            }           
        </script>  
    </head>
    <body style="align-items: center">
        <div id="page_bg"> <%-- style="width: 90, align: center"> --%>
            <div id="top_compartment">
                <div id="banner">
                    <a href=""><img src="images/OvGlycDBHeader.jpg" align="center"/></a>
                </div>
                <div id="nav">
                    <p>
                        <h3> <a href="index.jsp">Home</a> 
                            | <a href="identifiedProteins.jsp">Proteins</a> 
                            | <a href="identifiedPeptides.jsp">Peptides</a> 
                            | <a href="">Glycans</a> 
                            | <a href="">Glycoforms</a> 
                            | <a href="">Analyze</a> 
                            | <a href="identifiedSpectra.jsp">Spectra</a> 
                            | <a href="downloads.jsp">Downloads</a></h3>
                        <hr>
                    </p>
                </div>                
            </div>
            <div id="middle_compartment">
                <div id="middle_compartment_left-column">
                </div>
                <div id="middle_compartment_center-column">
                    <p class="text-content"> 
                        Glycosylation, particularly the N-linked glycosylation is known to be one of the most common post- and 
                        cotranslational protein modifications in eukaryotes. Occurring on the rough endoplasmic reticulum on 
                        nascent polypeptide synthesized, it has been observed to direct the eventual conformational structure 
                        and subsequent function of associated proteins. Involved in myriads of physiological processes, 
                        aberrant changes have as well been identified and implicated in several disease states.
                    </p>
                    <p>
                    <form action="indexPageSearchController.jsp" 
                          method="post" 
                          target="popup" 
                          onsubmit="window.open('', 
                                                this.target,
                                                'height=550,width=850,top=100,left=100,resizable=yes,scrollbars=yes,toolbar=yes,status=yes'); 
                                    return true;">
                            <select name="searchType">
                                <option value="Protein">Protein</option>
                                <option value="Peptide">Peptide</option>
                            </select>
                            <input type="text" name="searchItem"></input>
                            <input type="submit" value="Search"><br/>
                            <!-- <button id="searchButton" type="">Search</button><br/><!-- 
                            <!-- <input></input><button id="peptide_search_button" type="">Search</button><br/> -->
                        </form>
                    
                    <%--
                    http://www.yourhtmlsource.com/javascript/popupwindows.html
                    http://stackoverflow.com/questions/9498621/open-a-new-window-inside-same-page-in-a-small-size
                    
                    --%>
                        <br/><br/><br/><br/><br/><br/>
                    </p>
                </div>
                <div id="middle_compartment_right-column">
                </div>
            </div>
            <div id="footer_compartment">
                <div id="copyright">
                    <p>&copy; Copyright 2014</p>
                    <p id="date"></p>
                </div>
                <div id="contact">
                    <hr>
                    Department of Pathology | Johns Hopkins University | Baltimore | MD 21212
                </div>         
            </div>            
        </div>
        <script>
            document.getElementById("date").innerHTML = Date();
        </script>
    </body>
</html>
