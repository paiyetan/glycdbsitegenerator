/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package others;

/**
 *
 * @author paiyeta1
 */
public class GlycProteinMap {
    
    private int id;
    private String glycosite;
    private String proteinAccn;
    private String location;
    private int start;
    private int end;

    public GlycProteinMap(int id, String glycosite, String proteinAccn, String location) {
        this.id = id;
        this.glycosite = glycosite;
        this.proteinAccn = proteinAccn;
        this.location = location;
        setSequenceIndeces();
    }

    public int getId() {
        return id;
    }

    public String getGlycosite() {
        return glycosite;
    }

    public String getProteinAccn() {
        return proteinAccn;
    }

    public String getLocation() {
        return location;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
 
    private void setSequenceIndeces() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String[] locArr = location.split("\\.\\.\\.");
        start = Integer.parseInt(locArr[0]);
        end = Integer.parseInt(locArr[1]);       
    }
    
    
    
    
}
