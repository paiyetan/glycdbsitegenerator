<%-- 
    Document   : peptideResponse
    Created on : Oct 8, 2014, 12:40:05 PM
    Author     : paiyeta1
--%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Identified peptide (Glycosite)</title> 
        <link rel="stylesheet" href="bin/css/resultstyle.css">
        
        <script>
            var newwindow;
            function openNewWindow(url)
            {
                newwindow=window.open(url,'name', 'height=550,width=850, top=100, left=100, resizable=yes,scrollbars=yes,toolbar=yes,status=yes');
                if (window.focus) {newwindow.focus()}
                //newwindow.resizeTo(screen.availWidth,screen.availHeight);
            }           
        </script>  
    </head>
    <body>
        <div id="result">
            <%
              String glycosite = request.getParameter("searchItem");
              //load database...
              //make queries on database and place result in Java Objects...
              // glycosite
              //   ..mapped proteins...
              //   ..mapped spectra
              //   ..for each mapped spectrum, retrieve additional spectrum associated information
              //   ..
              LinkedList<String> mappedProtAccns = new LinkedList<String>();
              LinkedList<String> mappedSpectraIds = new LinkedList<String>();
              
              //access db for glycosite (peptide) associated information...
              Connection connection; 
              Statement statement;
              ResultSet resultSet; 
              String query;
              //try{
                  Class.forName("org.sqlite.JDBC");
                  connection = DriverManager.getConnection("jdbc:sqlite:" + 
                          "C:/Users/paiyeta1/Projects/cptacassociated/analysis/glyco/glycotriplicatesorbitr-2014.03.04/text/xGlycscan.tables/dbase/glycoSQLiteDB.db");
                  statement = connection.createStatement();  

                  // retrieve mapped protein accessions...
                  query = "SELECT * FROM glycosite_protein_map WHERE _glycosite_sequence='" + 
                            glycosite + "';"; 
                  resultSet = statement.executeQuery(query); 
                  while(resultSet.next()){
                      String mappedProtAccn = resultSet.getString("_protein_accession");
                      mappedProtAccns.add(mappedProtAccn);
                  }             

                  // retrieve mapped spectra Ids
                  query = "SELECT * FROM glycosite_spectrum_map WHERE _glycosite='" + 
                            glycosite + "';";
                  resultSet = statement.executeQuery(query); 
                  while(resultSet.next()){
                      String mappedSpectrumId = resultSet.getString("_spectrumId");
                      mappedSpectraIds.add(mappedSpectrumId);
                  }             

                  resultSet.close();
                  statement.close();
                  connection.close();
              //} catch (Exception ex){
              //    ex.printStackTrace();
              //}
              
            %>
            
        <table>
            <tr class="header">
                <td colspan="2"><h3>Identified peptide (Glycosite): <%= glycosite %></h3></td></tr>
            <tr valign="top">
                <td><strong>Mapped protein(s):</strong></td>
                <td>
                    <% for(String mappedProtAccn: mappedProtAccns){ 
                        String queryString = "javascript:openNewWindow('proteinResponse.jsp?searchItem=" + mappedProtAccn + "');";
                    %>
                        <a href=<%= queryString %> ><%= mappedProtAccn %></a><br>
                    <% } %>
                </td>
            </tr>
            <tr valign="top">
                <td><strong>Mapped spectr(um)a:</strong></td>
                <td>
                    <%= mappedSpectraIds.size() %> spectr(um)a mapped<br>
                    <% for(String mappedSpectrumId: mappedSpectraIds){ 
                        String queryString = "javascript:openNewWindow('spectrumResponse.jsp?spectrumId=" + mappedSpectrumId + "');";                 
                    %>
                        <a href=<%= queryString %> ><%= mappedSpectrumId %></a><br>
                    <% } %>
                    
                </td>
            </tr>
        </table>
        </div>
        <div id="fig">
            <hr>            
        </div>
        
    </body>
</html>
